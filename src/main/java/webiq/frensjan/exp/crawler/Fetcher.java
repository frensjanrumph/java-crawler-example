package webiq.frensjan.exp.crawler;

import java.io.IOException;
import java.net.URL;

/**
 * Fetch a document from a given {@link URL}
 *
 * @param <D> The type of document that is fetched.
 */
public interface Fetcher<D> {

    D fetch(URL url) throws IOException;

}
