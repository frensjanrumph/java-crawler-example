package webiq.frensjan.exp.crawler;

import java.util.stream.Stream;

public interface Tokenizer {

    /**
     * Splits the given text into a stream of tokens (words).
     */
    Stream<String> tokenize(String text);

}
