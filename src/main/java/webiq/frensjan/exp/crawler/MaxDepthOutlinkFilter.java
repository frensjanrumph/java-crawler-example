package webiq.frensjan.exp.crawler;

import java.net.URL;

/**
 * Limits outlinks to a given maximum distance (from the seed URL).
 */
public class MaxDepthOutlinkFilter implements OutlinkFilter {

    private final int maxDistance;

    public MaxDepthOutlinkFilter(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public boolean test(URL outlink, int distance) {
        return distance <= maxDistance;
    }

}
