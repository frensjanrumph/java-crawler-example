package webiq.frensjan.exp.crawler.wikipedia;

import webiq.frensjan.exp.crawler.TermFrequency;
import webiq.frensjan.exp.crawler.WordCount;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The main entry point for the crawl of wikipedia. It parses arguments and then delegates to {@link WikipediaCrawler}
 * to do the actual work.
 */
public class CrawlWikipediaTool {

    private static final int DEFAULT_DISTANCE = 2;
    private static final Duration DEFAULT_MAX_DURATION = Duration.ofMinutes(1);

    public static void main(String[] args) {
        if (args.length==0) {
            exitWithError("Not enough arguments");
        }

        if (args[0].equals("help")) {
            printUsage();
            System.exit(0);
        }

        URL seedUrl = parseUrl(args[0]);
        int maximumDistance = args.length >= 2 ? parseMaximumDistance(args[1]):DEFAULT_DISTANCE;
        Duration maxDuration = args.length >= 3 ? parseDuration(args[2]):DEFAULT_MAX_DURATION;

        System.out.println("Starting crawl with seed URL: " + seedUrl);
        System.out.println("Crawl is limited to distance " + maximumDistance);
        System.out.println("Crawl is limited to duration of " + maxDuration);
        System.out.println();

        LocalDateTime start = LocalDateTime.now();
        WordCount wordCount = new WikipediaCrawler(seedUrl, maximumDistance, maxDuration).crawl();

        System.out.println();
        System.out.println("Crawl done in " + Duration.between(start, LocalDateTime.now()));
        System.out.println();

        System.out.println("Analyzed: " + wordCount.getDocumentCount() + " documents");
        System.out.println();
        System.out.println("The most comment terms by frequency:");
        printTermFrequencies(wordCount.mostCommonTermsByFrequency(20));
        System.out.println();

        System.out.println("The most comment terms by document frequency:");
        printTermFrequencies(wordCount.mostCommonTermsByDocumentFrequency(20));
    }

    private static void printTermFrequencies(List<TermFrequency> termFrequencies) {
        for (TermFrequency termFrequency : termFrequencies) {
            System.out.println(" - " + termFrequency.getTerm() + " = " + termFrequency.getFrequency());
        }
    }

    private static URL parseUrl(String arg) {
        try {
            return new URL(arg);
        } catch (MalformedURLException e) {
            exitWithError("Unable to parse seed url from input '" + arg + "'");
            throw new RuntimeException(e);
        }
    }

    private static int parseMaximumDistance(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            exitWithError("Unable to parse maximum distance from input '" + arg + "'");
            throw e;
        }
    }

    private static Duration parseDuration(String arg) {
        try {
            return Duration.parse(arg);
        } catch (Exception e) {
            exitWithError("Unable to parse deadline from input '" + arg + "'");
            throw e;
        }
    }

    private static void printUsage() {
        System.out.println("" +
                "A simple crawler in Java. It's arguments are:\n" +
                "\n" +
                "    {seed url} {maximum distance} {deadline}\n" +
                "\n" +
                " - seed url         (url)      = The URL to start the crawl with.\n" +
                " - maximum distance (number)   = The maximum number length of paths (through hyperlinks)\n" +
                "                                 to follow from the seed URL. Defaults to " + DEFAULT_DISTANCE + ".\n" +
                " - deadline         (ISO-8601) = The maximum amount of time to take for the crawl\n" +
                "                                 in ISO-8601 duration notation. Defaults to " + DEFAULT_MAX_DURATION + "."
        );
    }

    private static void exitWithError(String message) {
        System.out.print("" +
                "Unable to start crawler: \n"
                + "\n"
                + "    Error: " + message + "\n"
                + "\n"
                + "Usage: ");
        printUsage();
        System.exit(1);
    }
}
