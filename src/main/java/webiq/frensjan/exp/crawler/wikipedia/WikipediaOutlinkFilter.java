package webiq.frensjan.exp.crawler.wikipedia;

import webiq.frensjan.exp.crawler.OutlinkFilter;

import java.net.URL;

/**
 * A simple filter tailored to wikipedia.
 * SHORTCUT: as always, this filter is no where near complete ... there'll be tons of garbage we're still pulling in.
 */
public class WikipediaOutlinkFilter implements OutlinkFilter {

    @Override
    public boolean test(URL outlink, int distance) {
        if (!outlink.getHost().equals("en.wikipedia.org")) {
            return false;
        }

        if (!outlink.getPath().startsWith("/wiki/")) {
            return false;
        }

        // finally make sure the path doesn't contain a colon
        // like /wiki/Category: or /wiki/Help:
        return !outlink.getPath().contains(":");
    }
}
