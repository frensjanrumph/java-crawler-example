package webiq.frensjan.exp.crawler.wikipedia;

import org.jsoup.Jsoup;
import webiq.frensjan.exp.crawler.Analyzer;
import webiq.frensjan.exp.crawler.Crawler;
import webiq.frensjan.exp.crawler.Fetcher;
import webiq.frensjan.exp.crawler.MaxDepthOutlinkFilter;
import webiq.frensjan.exp.crawler.NoFragmentsUrlFilter;
import webiq.frensjan.exp.crawler.OutlinkExtractor;
import webiq.frensjan.exp.crawler.OutlinkFilter;
import webiq.frensjan.exp.crawler.Tokenizer;
import webiq.frensjan.exp.crawler.WordCount;
import webiq.frensjan.exp.crawler.jsoup.JsoupFetcher;
import webiq.frensjan.exp.crawler.jsoup.JsoupOutlinkExtractor;
import webiq.frensjan.exp.crawler.jsoup.JsoupTextExtractor;

import java.net.URL;
import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

/**
 * A composition of various crawler components: e.g. a {@link Fetcher}, {@link OutlinkExtractor} and
 * {@link OutlinkFilter}s, {@link Analyzer}s, {@link Tokenizer} and {@link Predicate}s for token filtering, etc.
 * <br />
 * This class uses {@link Jsoup} based components to do "the work" and delegates the crawling to {@link Crawler}.
 * <br />
 * The result of a wikipedia crawl is a {@link WordCount} based on all the documents crawled given the seed url,
 * maximum distance and maximum duration.
 */
public class WikipediaCrawler {

    // SHORTCUT: a bunch of dependencies hardcoded together for the example use case
    // perhaps using dependency injection or something would be better
    // would also make this class testable ... if need be, it doesn't add that much functionality

    /**
     * The number of threads to crawl with.
     * SHORTCUT: make this configurable
     */
    private static final int THREADS = 4;

    private static final JsoupFetcher FETCHER = new JsoupFetcher();

    // SHORTCUT: this tokenizer deserves it's own class; for starters for testing, but also there are a lot of
    // options in tokenization that it deserves more options for composition / configuration
    private static final Pattern TOKEN_SPLIT_PATTERN = Pattern.compile("[\b\\s]");
    private static final Pattern TOKEN_CLEAN_PATTERN = Pattern.compile("[^\\w]+");
    private static final Tokenizer TOKENIZER = text ->
            TOKEN_SPLIT_PATTERN.splitAsStream(text)
                    .map(String::toLowerCase)
                    .map(token -> TOKEN_CLEAN_PATTERN
                            .matcher(token)
                            .replaceAll(" ")
                            .trim());

    // SHORTCUT: use a better list of stop words, e.g. load it from a nltk file
    private static final Set<String> STOP_WORDS = new HashSet<>(asList(
            ("" +
                    " and for the of to in is on as from that with was are this may" +
                    " which other have has can their its such where also were all" +
                    " but used see one been some")
                    .split(" ")
    ));

    // SHORTCUT: also token filtering should be factored out into a text analysis package
    private static final Predicate<String> IS_STOP_WORD = STOP_WORDS::contains;
    private static final Predicate<String> FILTER_SHORT_TOKENS = token -> token.length() > 2;
    private static final Predicate<String> TOKEN_FILTER =
            IS_STOP_WORD.negate()
                    .and(FILTER_SHORT_TOKENS);

    private final URL seedUrl;
    private final int maximumDistance;
    private final Duration maximumDuration;

    WikipediaCrawler(URL seedUrl, int maximumDistance, Duration maximumDuration) {
        this.seedUrl = seedUrl;
        this.maximumDistance = maximumDistance;
        this.maximumDuration = maximumDuration;
    }

   protected WordCount crawl() {
        MaxDepthOutlinkFilter limitDepth = new MaxDepthOutlinkFilter(maximumDistance);
        NoFragmentsUrlFilter noFragments = new NoFragmentsUrlFilter();
        WikipediaOutlinkFilter limitToWikipedia = new WikipediaOutlinkFilter();

        Collection<OutlinkFilter> outlinkFilters = asList(
                limitDepth,
                noFragments,
                limitToWikipedia
        );

        JsoupOutlinkExtractor outlinkExtractor = new JsoupOutlinkExtractor(outlinkFilters);

        WordCount wordCount = new WordCount(
                TOKENIZER,
                TOKEN_FILTER
        );

        JsoupTextExtractor textExtractor = new JsoupTextExtractor(wordCount);

        new Crawler<>(FETCHER, outlinkExtractor, textExtractor)
                .crawl(seedUrl, maximumDuration, THREADS);

        return wordCount;
    }
}
