package webiq.frensjan.exp.crawler;

import java.net.URL;
import java.util.List;

public interface OutlinkExtractor<D> {

    /**
     * Extract a list of outlinks ({@link URL}) from the given document.
     *
     * @param document The document to extract outlinks from.
     * @param url      The url of the document.
     * @param distance The distance of the document in relation to the seed URL.
     * @return A list of outlinks.
     */
    List<URL> extract(D document, URL url, int distance);

}
