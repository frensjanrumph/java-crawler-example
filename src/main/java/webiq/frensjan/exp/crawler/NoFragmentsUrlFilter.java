package webiq.frensjan.exp.crawler;

import java.net.URL;

/**
 * Ignores any URL's that are fragments.
 * SHORTCUT: this should have actually been an outlink _normalizer_ ...
 */
public class NoFragmentsUrlFilter implements OutlinkFilter {

    @Override
    public boolean test(URL outlink, int distance) {
        return outlink.getRef()==null;
    }

}
