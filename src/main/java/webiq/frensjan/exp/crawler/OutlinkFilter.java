package webiq.frensjan.exp.crawler;

import java.net.URL;

public interface OutlinkFilter {

    /**
     * Test if an outlink is to be crawled or not.
     *
     * @param outlink  The outlink to test.
     * @param distance It's distance from the seed URL.
     * @return {@code true} when to crawl the outlink or {@code false} if not.
     */
    boolean test(URL outlink, int distance);

}
