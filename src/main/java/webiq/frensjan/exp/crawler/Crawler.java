package webiq.frensjan.exp.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Crawler<D> {

    private static final Logger LOG = LoggerFactory.getLogger(Crawler.class);

    /**
     * A set of URLs already visited. The URLs are encoded as {@link String} because {@link URL} performs DNS lookups
     * in equals/hashCode.
     */
    private final Set<String> visited = ConcurrentHashMap.newKeySet();

    /**
     * Fetching documents is delegated to a {@link Fetcher}.
     */
    private final Fetcher<D> fetcher;

    /**
     * Extracting outlinks is delegates to a {@link OutlinkExtractor}.
     */
    private final OutlinkExtractor<D> outlinkExtractor;

    /**
     * Documents crawled are fed into the analyzer accompanied by their URL.
     */
    private final Analyzer<D> analyzer;

    public Crawler(Fetcher<D> fetcher, OutlinkExtractor<D> outlinkExtractor, Analyzer<D> analyzer) {
        this.fetcher = fetcher;
        this.outlinkExtractor = outlinkExtractor;
        this.analyzer = analyzer;
    }

    /**
     * Perform a single crawl with the given seed url, with a maximum duration using (at most) the given number of
     * threads.
     * Note that if the seed URL is already crawled, this is essentially a no-op.
     */
    public void crawl(URL seedUrl, Duration maximumDuration, int threads) {
        new CrawlRun(seedUrl, maximumDuration, threads).run();
    }

    /**
     * A single run of the crawler for a seed URL.
     */
    private class CrawlRun {

        private final Queue<Future<?>> pending = new ConcurrentLinkedQueue<>();

        private final URL seedUrl;
        private final LocalDateTime deadline;
        private final int threads;

        private ExecutorService executor;
        private LinkedBlockingQueue<Runnable> queue;

        CrawlRun(URL seedUrl, Duration maxDuration, int threads) {
            this.seedUrl = seedUrl;
            this.deadline = LocalDateTime.now().plus(maxDuration);
            this.threads = threads;
        }

        /**
         * Run the crawl with the seed URL.
         */
        private void run() {
            queue = new LinkedBlockingQueue<>();
            executor = new ThreadPoolExecutor(threads, threads, 0L, TimeUnit.MILLISECONDS, queue);
            try {
                this.submit(seedUrl, 0);
                waitForCompletionOrDeadline();
            } finally {
                executor.shutdownNow();
            }
        }

        /**
         * Wait for pending to be empty or the deadline is exceeded.
         * Each Future that is added to pending is waited for.
         */
        @SuppressWarnings("java:S135")
        private void waitForCompletionOrDeadline() {
            try {
                while (!pending.isEmpty()) {
                    LOG.debug("The crawler has {} milliseconds remaining and {} URLs in progress / pending",
                            getMillisRemaining(), queue.size());

                    // check the deadline
                    if (getMillisRemaining() < 0) {
                        LOG.info("Deadline exceeded, terminating crawler");
                        break;
                    }

                    // wait for a new future
                    Future<?> future = pending.poll();
                    if (future==null) {
                        LOG.info("All URL's crawled, terminating crawler");
                        break;
                    }

                    future.get(getMillisRemaining(), TimeUnit.MILLISECONDS);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOG.info("Crawler interrupted, terminating");
            } catch (ExecutionException e) {
                LOG.info("Unexpected exception in crawler, terminating", e);
            } catch (TimeoutException e) {
                LOG.info("Deadline exceeded, terminating crawler");
            }
        }

        private long getMillisRemaining() {
            return Duration.between(LocalDateTime.now(), deadline).toMillis();
        }

        private void submit(URL url, int distance) {
            boolean isNewUrl = visited.add(url.toExternalForm());
            if (isNewUrl) {
                pending.add(executor.submit(() -> this.process(url, distance)));
            }
        }

        /**
         * <ol>
         * <li>Fetch the resource for the given URL,</li>
         * <li>feed the resource to the analyser</li>
         * <li>and extract outlinks and schedule them.</li>
         * </ol>
         */
        private void process(URL url, int distance) {
            try {
                D document = fetcher.fetch(url);
                analyzer.consume(url, document);
                outlinkExtractor.extract(document, url, distance)
                        .forEach(u -> submit(u, distance + 1));
            } catch (IOException e) {
                LOG.warn("Unable to fetch url " + url, e);
            }
        }
    }
}
