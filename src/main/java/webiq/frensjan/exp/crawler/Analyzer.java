package webiq.frensjan.exp.crawler;

import java.net.URL;

/**
 * Consume a document fetched from the given url for analysis.
 */
public interface Analyzer<D> {

    void consume(URL url, D document);

}
