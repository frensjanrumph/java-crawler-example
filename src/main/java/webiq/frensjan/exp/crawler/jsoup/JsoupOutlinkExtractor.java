package webiq.frensjan.exp.crawler.jsoup;

import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webiq.frensjan.exp.crawler.OutlinkExtractor;
import webiq.frensjan.exp.crawler.OutlinkFilter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Extract absolute outlinks with {@link org.jsoup.Jsoup}. Empty outlinks are filtered out and then filtered by the
 * given {@link OutlinkFilter}s in  {@link #filters}.
 */
public class JsoupOutlinkExtractor implements OutlinkExtractor<Document> {

    private static final Logger LOG = LoggerFactory.getLogger(JsoupOutlinkExtractor.class);

    private final List<OutlinkFilter> filters = new ArrayList<>();

    public JsoupOutlinkExtractor(Collection<OutlinkFilter> filters) {
        this.filters.addAll(filters);
    }

    @Override
    public List<URL> extract(Document document, URL url, int distance) {
        int nextDistance = distance + 1;

        List<URL> outlinks = document.select("a").stream()
                .map(anchor -> anchor.absUrl("href"))
                .map(String::trim)
                .filter(href -> !href.isEmpty())
                .flatMap(href -> parseOutlink(url, href))
                .flatMap(outlink -> filterOutlink(nextDistance, outlink))
                .collect(toList());

        LOG.debug("Extracted {} outlinks from {}", outlinks.size(), url);

        return outlinks;
    }

    private Stream<URL> parseOutlink(URL url, String href) {
        try {
            return Stream.of(new URL(href));
        } catch (MalformedURLException e) {
            LOG.warn("Encountered malformed URL {} in document at URL {}", href, url);
            return Stream.empty();
        }
    }

    private Stream<URL> filterOutlink(int nextDistance, URL outlink) {
        for (OutlinkFilter filter : filters) {
            if (!filter.test(outlink, nextDistance)) {
                LOG.trace("Outlink {} was rejected by {}", outlink, filter);
                return Stream.empty();
            }
        }

        return Stream.of(outlink);
    }

}
