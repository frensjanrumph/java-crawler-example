package webiq.frensjan.exp.crawler.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webiq.frensjan.exp.crawler.Fetcher;

import java.io.IOException;
import java.net.URL;

/**
 * Fetch {@link Document}s with {@link Jsoup}.
 */
public class JsoupFetcher implements Fetcher<Document> {

    private static final Logger LOG = LoggerFactory.getLogger(JsoupFetcher.class);

    // SHORTCUT: make this configurable
    private static final int FETCH_TIMEOUT_MILLIS = 60 * 1000;

    @Override
    public Document fetch(URL url) throws IOException {
        LOG.debug("Fetching {}", url);
        return Jsoup.parse(url, FETCH_TIMEOUT_MILLIS);
    }

}
