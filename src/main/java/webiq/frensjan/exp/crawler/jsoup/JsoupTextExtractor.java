package webiq.frensjan.exp.crawler.jsoup;

import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webiq.frensjan.exp.crawler.Analyzer;

import java.net.URL;

/**
 * Uses {@link org.jsoup.Jsoup} to extract text from the body of a (HTML) {@link Document}.
 */
public class JsoupTextExtractor implements Analyzer<Document> {

    private static final Logger LOG = LoggerFactory.getLogger(JsoupTextExtractor.class);

    private final Analyzer<String> downstream;

    public JsoupTextExtractor(Analyzer<String> downstream) {
        this.downstream = downstream;
    }

    @Override
    public void consume(URL url, Document document) {
        String text = document.body().text();
        LOG.debug("Extract {} characters from {}", text.length(), url);
        downstream.consume(url, text);
    }
}
