package webiq.frensjan.exp.crawler;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 * An analyzer that consumes text, splits it into words (tokens) and counts these, both in total and per document.
 * It also maintains a count of the number of documents analyzed.
 */
public class WordCount implements Analyzer<String> {

    private final AtomicLong documentCount = new AtomicLong();
    private final Map<String, Integer> wordFrequencies = new ConcurrentHashMap<>();
    private final Map<String, Integer> documentFrequencies = new ConcurrentHashMap<>();

    private final Tokenizer tokenizer;
    private final Predicate<String> termFilter;

    public WordCount(Tokenizer tokenizer, Predicate<String> termFilter) {
        this.tokenizer = tokenizer;
        this.termFilter = termFilter;
    }

    @Override
    public void consume(URL url, String text) {
        documentCount.incrementAndGet();

        Set<String> documentTokens = new HashSet<>();

        tokenizer
                .tokenize(text)
                .filter(termFilter)
                .peek(documentTokens::add)
                .forEach(token -> wordFrequencies.merge(token, 1, Integer::sum));

        documentTokens
                .forEach(token -> documentFrequencies.merge(token, 1, Integer::sum));
    }

    public long getDocumentCount() {
        return documentCount.get();
    }

    public List<TermFrequency> mostCommonTermsByFrequency(int count) {
        return topK(wordFrequencies)
                .limit(count)
                .collect(toList());
    }

    /**
     * Computes the top-k terms by their document frequency (in how many documents the terms occur). This top-k
     * excludes terms that have a count that is the same across the top-k. E.g. if the top 20 is requested, and this
     * would return the words that occur in every document, these would be 'shaved' of. This process is repeated until
     * at least one term in the top-k has a different frequency than the others or there are no terms left.
     *
     * @param count The number of terms to return.
     * @return The term frequencies by document count.
     */
    public List<TermFrequency> mostCommonTermsByDocumentFrequency(int count) {
        long frequencyToSkip = getDocumentCount();
        List<TermFrequency> termFrequencies;

        while (true) {
            termFrequencies = topK(documentFrequencies)
                    .filter(removeFiltersWithFrequency(frequencyToSkip))
                    .limit(count)
                    .collect(toList());

            if (termFrequencies.isEmpty()) {
                break;
            } else if (!allFrequenciesEqual(termFrequencies)) {
                break;
            }

            frequencyToSkip = termFrequencies.get(0).getFrequency();
        }

        return termFrequencies;
    }

    private Predicate<TermFrequency> removeFiltersWithFrequency(long countToSkip2) {
        return termFrequency -> termFrequency.getFrequency() < countToSkip2;
    }

    private boolean allFrequenciesEqual(List<TermFrequency> termFrequencies) {
        return termFrequencies.stream()
                .map(TermFrequency::getFrequency)
                .distinct()
                .count()==1;
    }

    private Stream<TermFrequency> topK(Map<String, Integer> entries) {
        return entries.entrySet().stream()
                .map(TermFrequency::new)
                .sorted(comparing(TermFrequency::getFrequency).reversed());
    }

}
