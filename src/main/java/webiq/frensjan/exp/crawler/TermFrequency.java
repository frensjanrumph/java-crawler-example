package webiq.frensjan.exp.crawler;

import java.util.Map;
import java.util.Objects;

/**
 * A pair of a term (word) and the frequency (e.g. how often the term occurred in a corpus or in how many documents in
 * the corpus).
 */
public class TermFrequency {

    private final String term;
    private final int frequency;

    public TermFrequency(Map.Entry<String, Integer> entry) {
        this(entry.getKey(), entry.getValue());
    }

    public TermFrequency(String term, int frequency) {
        this.term = term;
        this.frequency = frequency;
    }

    public String getTerm() {
        return term;
    }

    public int getFrequency() {
        return frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this==o) return true;
        if (!(o instanceof TermFrequency)) return false;
        TermFrequency that = (TermFrequency) o;
        return frequency==that.frequency &&
                Objects.equals(term, that.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(term, frequency);
    }

    @Override
    public String toString() {
        return "TermFrequency{" +
                "term='" + term + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
