# java-crawler-example

This is an implementation of a simple crawler in java. It is a reference for interview assignments at Web-IQ.

> The assignment for software engineering applicants is the following: develop a ’tool’ that:
>   - given a seed URL, for example https://en.wikipedia.org/wiki/Open-source_intelligence
>   - computes the most important terms
>   - before a certain deadline (e.g. 1 minuut counting after start)
>   - and a maximum number of ’steps’ from the seed url
>
> The ’tool’ does not have to be production grade; use it mainly to show your software development skills. Perhaps you
> can use TODO markers to highlight deliberate ’shortcuts’ in order for you to focus on one or two key areas. Existing
> libraries can be of assistance; but use them wisely, we are interested in how you write software.

As you can see, the assignment is rather 'open'. This is on purpose.

---

Notes on my implementation: I've developed this in around 4 hours. The main open point is tests. That said, I think the
code is reasonably "open for testing". I took a lot of shortcuts around the text analysis part. The functionality is (
somewhat) there, but could be expanded on in terms of composition/configuration and quality.